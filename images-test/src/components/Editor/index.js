import React, { Component } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';
import { styles } from './styles.scss';

import Childs from '../Childs';



export default class Editor extends Component {
  _onChange(event) {
    const { updateText, params: {id} } = this.props;
    const text = event.target.value;
    updateText(parseInt(id, 10), text);
  }

  render() {
    const { notes, params: {id} } = this.props;
    const note = notes.find((item) => {
      return item.get('id') == id;
    });

    const text = note ? note.get('text') : 'not found';
    return (
      <section className={styles}>
        <textarea
          className="textarea"
          value={text}
          onChange={this._onChange.bind(this)}
          >
        </textarea>
      </section>
    );
  }
}
